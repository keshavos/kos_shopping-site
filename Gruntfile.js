// Grunt tasks
module.exports = function(grunt) {

    "use strict";

    // Project configuration.
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        banner: '/*!\n' +
            '* <%= pkg.name %> - v<%= pkg.version %> - MIT LICENSE <%= grunt.template.today("yyyy-mm-dd") %>. \n' +
            '* @author <%= pkg.author %>\n' +
            '*/\n',

        clean: {
            dist: ['src']
        },

        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            gruntfile: {
                src: 'Gruntfile.js'
            },
            app: {
                src: ['app/modules/**/*.js']
            }
        },

        exec: {
            bowerInstaller: 'bower-installer'
        },

        concat: {
            options: {
                banner: '<%= banner %>',
                stripBanners: false
            },
            base: {
                src: [
                    // Angular Project Dependencies,
                    'app/app.js',
                    'app/app.config.js',
                    'app/modules/**/*Module.js',
                    'app/modules/**/*Route.js',
                    'app/modules/**/*Ctrl.js',
                    'app/modules/**/*Service.js',
                    'app/modules/**/*Directive.js'
                ],
                dest: 'app/assets/js/<%= pkg.name %>-appbundle.js'
            },
            build: {
                src: [
                    // Angular Project Dependencies,
                    'app/assets/libs/angular/angular.js',
                    'app/assets/libs/**/*.js'

                ],
                dest: 'app/assets/js/<%= pkg.name %>-angularbundle.js'
            }
        },

        uglify: {
            options: {
                banner: '<%= banner %>',
                report: 'min'
            },
            base: {
                src: ['<%= concat.base.dest %>'],
                dest: 'app/assets/js/<%= pkg.name %>-angscript.min.js'
            },
            basePlugin: {
                src: ['src/plugins/**/*.js'],
                dest: 'app/assets/js/plugins/',
                expand: true,
                flatten: true,
                ext: '.min.js'
            }
        },

        connect: {
            server: {
                options: {
                    keepalive: true,
                    port: 4000,
                    base: '.',
                    hostname: 'localhost',
                    debug: true,
                    livereload: true,
                    open: true
                }
            }
        },

        concurrent: {
            tasks: ['connect', 'watch:app', 'watch:sass'],
            options: {
                logConcurrentOutput: true
            }
        },

        watch: {
            app: {
                files: '<%= jshint.app.src %>',
                tasks: ['jshint:app'],
                options: {
                    livereload: 12345
                }
            },
            sass: {
                files: 'app/modules/*/scss/**/*.scss',
                tasks: ['sass:dev'],
                options: {
                    livereload: 12346
                }
            }
        },

        sass: {
            dev: {
                options: {
                    outputStyle: 'expanded'
                },
                files: {
                    'app/assets/css/main.css': 'app/assets/scss/main.scss'
                }
            }
        },

        injector: {
            options: {},
            dev: {
                files: {
                    'index.html': [
                        'bower.json',
                        'app/app.js',
                        'app/app.config.js',
                        'app/modules/*/*.module.js',
                        'app/**/*.config.js',
                        'app/**/*.js',
                        '!app/**/*.spec.js'
                    ]
                }
            },
            production: {
                files: {
                    'index.html': [
                        'app/assets/css/**/*.css',
                        'app/assets/js/*.js'
                    ]

                }
            },
            karma_bower_dependencies: {
                options: {
                    transform: function(filepath) {
                        return '\'' + filepath.substring(1) + '\',';
                    },
                    bowerPrefix: 'bower-',
                    wiredepOptions: {
                        // Overrides necessary to change injector order
                        overrides: {
                            "outlayer": {
                                "main": [
                                    "item.js",
                                    "outlayer.js"
                                ]
                            }
                        },
                    }
                },
                files: {
                    'karma.conf.js': [
                        'bower.json'
                    ]
                }
            },
            karma_dependencies: {
                options: {
                    transform: function(filepath) {
                        return '\'' + filepath.substring(1) + '\',';
                    }
                },
                files: {
                    'karma.conf.js': [
                        'app/modules/*/*.module.js',
                        'app/app.js',
                        'app/modules/*/config/*.js',
                        'app/modules/*/controllers/*.js',
                        'app/modules/*/directives/*.js',
                        'app/modules/*/services/*.js',
                        'app/modules/*/tests/unit/*.spec.js',
                    ]
                }
            }
        },

        karma: {
            unit: {
                configFile: 'karma.conf.js',
                singleRun: false
            }
        },

        ngtemplates: {
            app: {
                src: 'app/modules/**/*.html',
                dest: 'app/assets/js/templates.js',
                options: {
                    module: '<%= pkg.name %>',
                    root: 'app/',
                    standAlone: false
                }
            }
        },

        ngdocs: {
            options: {
                dest: 'docs/',
                scripts: [
                    'src/bower_components/angular/angular.js',
                    'src/bower_components/angular-animate/angular-animate.js'
                ],
                html5Mode: true,
                startPage: '/docs',
                title: 'Shopping Website',
                titleLink: '/api',
                bestMatch: true
            },
            api: {
                src: [
                    '!src/bower_components/**.*.js',
                    'app/**/*.module.js',
                    'app/**/*.js',
                    '!app/modules/*/tests/unit/**/*.spec.js'
                ],
                title: 'Shopping Website SPA Documentation'
            }
        }

    });

    require('time-grunt')(grunt);
    require('load-grunt-tasks')(grunt);

    // Making grunt default to force in order not to break the project if something fail.
    // grunt.option('force', true);

    // Register grunt tasks
    grunt.registerTask("build", [
        "jshint",
        "exec",
        "concat",
        "ngtemplates",
        "injector:production",
        "concurrent",
        "clean"
    ]);

    // Development task(s).
    grunt.registerTask('dev', ['injector:dev', 'concurrent']);
    grunt.registerTask('default', ['injector:dev', 'concurrent']);

};
