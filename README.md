Shopping website
=============
Task
-------------------------------------------
[Develop a responsive website for a clothing retailer](task/Front_End_Case_Study.pdf)

The app displays a list of items on launch. The inventory stock of the items is served through a [static json file](app/modules/product/mocks/product-inventory.json).

On hover on a product tile, a 'Buy' button is shown allowing the user to add it to their shopping cart.
If the inventory stock is 0, the 'Buy' button is disabled - which prevents the user from adding the item to the cart.

The button on the top right corner shows the current total price of the all the items added to the cart.

Clicking on the 'Cart total' button navigates to the view cart page which shows a grouped view of all the items and the quantity of items added to the cart.

The user is able to decrement or increment the quantity of any item through the '-' or '+' buttons respectively. Checks are added so that the user will not be able to add more quantities than the available inventory stock for each product.

Specified Use cases and their implementation status:
-------------------------------------------

1. As a User I can add a product to my shopping cart.
2. As a User I can remove a product from my shopping cart.
3. As a User I can view the total price for the products in my shopping cart.
4. ~~As a User I can apply a voucher to my shopping cart.~~
5. ~~As a User I can view the total price for the products in my shopping cart with discounts applied.~~
6. ~~As a User I am alerted when I apply an invalid voucher to my shopping cart.~~
7. As a User I am unable to Out of Stock products to the shopping cart.


Tech stack
-------------------------------------------
* AngularJS v1.4.8 and Angular Material v1.1.10
* Scaffolded using [generator-angm](https://www.npmjs.com/package/generator-angm)
* Build tool - Grunt. Few Grunt tasks from the generator have been modified and few tasks added
* Tests - Jasmine. Test runner - Karma
* Test coverage tool - Istanbul
* Documentation tool - ngdocs

Building and running on a development instance
-------------------------------------------
* Clone repository.
* Checkout to `master` branch. Make sure you're on the latest commit of master.
* Run `npm install` to install all the required node modules required for setting up the environment

Note: This should automatically run `bower install`. Ensure that this has been run by checking the `src/bower_components` to
  make sure all the frontend libraries have been downloaded

Running the app locally
-------------------------------------------
* Run `npm start` from the command line, at the root of the project
This should open the app on your default browser. The app should be running at `http://127.0.0.1:4000/`
The default page is currently set to `http://127.0.0.1:4000/#!/product/list` which displays the list of all products

Running development tasks
-------------------------------------------
* `npm start` - Starts and opens the app on the default browser
* `npm test` - Runs the test suite
* `grunt ngdocs` - Generates the documentation and updates the `docs/`

Tests Coverage
-------------------------------------------
- Unit tests are available in the `app/modules/*/tests/unit/` for each module.
- A coverage plugin has been added to easily visualise the test coverage for each file, grouped by - `config`, `controllers`, `directives`, `filters`, `services`.
- To access test coverage page, navigate to `http://127.0.0.1:4000/coverage/PhantomJS 2.1.1 (Mac OS X 0.0.0)/`
(Please ensure that the app is running with the `grunt dev` command)

Code Documentation
-------------------------------------------
- Documentation is automatically generated through the docblocks on each method
- To view the documentation, navigate to `http://127.0.0.1:4000/docs/` and click on the Title

Further Enhancements
-------------------------------------------
- The visual elements of the site are very basic without the use of any fancy transitions for user interaction with the UI.
- Some of the use cases have not been covered
- Test coverage is not complete
