NG_DOCS={
  "sections": {
    "api": "Shopping Website SPA Documentation"
  },
  "pages": [
    {
      "section": "api",
      "id": "app",
      "shortName": "app",
      "type": "overview",
      "moduleName": "app",
      "shortDescription": "Main module of the application.",
      "keywords": "api app application cart core data main md module nganimate ngaria ngmaterial ngmessages ngresource ngsanitize overview product router table ui",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "app.cart",
      "shortName": "app.cart",
      "type": "overview",
      "moduleName": "app.cart",
      "shortDescription": "The Cart module",
      "keywords": "api app cart module overview",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "app.cart.config:cartRoutes",
      "shortName": "cartRoutes",
      "type": "object",
      "moduleName": "app.cart",
      "shortDescription": "The config block which defines all the routes within the Cart module",
      "keywords": "api app block cart config defines module object routes",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "app.cart.controllers:CartListController",
      "shortName": "CartListController",
      "type": "controller",
      "moduleName": "app.cart",
      "shortDescription": "Controller bound to the list of all the cart items",
      "keywords": "api app baskethelper bound cart controller controllers items list",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "app.cart.directives:cartTotalButton",
      "shortName": "cartTotalButton",
      "type": "directive",
      "moduleName": "app.cart",
      "shortDescription": "A button which displays the current Cart total",
      "keywords": "$state api app button cart click current directive directives displays factory function list navigate navigatetocartlist product router shopping total ui view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "app.core",
      "shortName": "app.core",
      "type": "overview",
      "moduleName": "app.core",
      "shortDescription": "The Core module",
      "keywords": "api app core module overview",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "app.core.config:coreRoutes",
      "shortName": "coreRoutes",
      "type": "object",
      "moduleName": "app.core",
      "shortDescription": "The config block of the core module which defines all the routes within the module",
      "keywords": "api app block config core defines module object routes",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "app.core.controllers:MainController",
      "shortName": "MainController",
      "type": "controller",
      "moduleName": "app.core",
      "shortDescription": "The main controller which displays a list of all items",
      "keywords": "api app controller controllers core displays factory fetch function init items list load main product",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "app.core.directives:closeModal",
      "shortName": "closeModal",
      "type": "directive",
      "moduleName": "app.core",
      "shortDescription": "A button directive that closes the $mdDialog modal",
      "keywords": "$mddialog api app button call cance close closemodalfn closes core dialog directive directives function modal window",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "app.core.factory:ToastHelper",
      "shortName": "ToastHelper",
      "type": "service",
      "moduleName": "app.core",
      "shortDescription": "A helper factory related to displaying &#39;toast&#39; messages across the application",
      "keywords": "$mdtoast api app application core displaying factory function helper message messages ngmaterial service showtoast simple toast",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "app.core.run",
      "shortName": "app.core.run",
      "type": "object",
      "moduleName": "app.core",
      "shortDescription": "Adds run blocks to this module",
      "keywords": "$location $mdmedia $rootscope $state $stateparams add adds api app blocks change convenience core elements error handle hide loader material module ng object router runpagetransitions start success todo ui ui-router visual",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "app.core:configBlock",
      "shortName": "configBlock",
      "type": "object",
      "moduleName": "app",
      "shortDescription": "Config and run block",
      "keywords": "api app block config core object",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "app.product",
      "shortName": "app.product",
      "type": "overview",
      "moduleName": "app.product",
      "shortDescription": "The Product module",
      "keywords": "api app module overview product",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "app.product.config:productRoutes",
      "shortName": "productRoutes",
      "type": "object",
      "moduleName": "app.product",
      "shortDescription": "Config block which defines all the routes within the &#39;Product&#39; module",
      "keywords": "api app block config defines module object product routes",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "app.product.controllers:ProductViewController",
      "shortName": "ProductViewController",
      "type": "controller",
      "moduleName": "app.product",
      "shortDescription": "Controller bound to the view to display a single Product",
      "keywords": "api app bound controller controllers display product single view",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "app.product.directives:addToBasket",
      "shortName": "addToBasket",
      "type": "directive",
      "moduleName": "app.product",
      "shortDescription": "A directive button which adds the specified product along with the quantity to the Basket",
      "keywords": "adds api app basket button core directive directives factory product quantity",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "app.product.directives:productTile",
      "shortName": "productTile",
      "type": "directive",
      "moduleName": "app.product",
      "shortDescription": "A tile which displays the &quot;closed state&quot; of a product",
      "keywords": "api app closed directive directives displays ea factory product tile",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "app.product.factory:BasketHelper",
      "shortName": "BasketHelper",
      "type": "service",
      "moduleName": "app.product",
      "shortDescription": "A helper which deals with all &#39;Basket&#39; related functionality",
      "keywords": "adds addtobasket api app basket baskettotal checks core deals factory function functionality getbasket getbaskettotal getbaskettotalquantity getproductfrombasket helper item items list number object passes product quantity removed removefrombasket removes requirements returns service total updatebasket updatebaskettotal updates",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "app.product.factory:ProductHelper",
      "shortName": "ProductHelper",
      "type": "service",
      "moduleName": "app.product",
      "shortDescription": "A helper factory which serves product related data from the mocks file",
      "keywords": "$http $q api app data factory file function getproduct getproductslist helper inventory list matched mocks ng object product returns serves service",
      "isDeprecated": false
    },
    {
      "section": "api",
      "id": "app.product.factory:ProductModals",
      "shortName": "ProductModals",
      "type": "service",
      "moduleName": "app.product",
      "shortDescription": "A modals helper factory for the Product module",
      "keywords": "api app detail dialog factory function helper modals module opens product service viewproduct window",
      "isDeprecated": false
    }
  ],
  "apis": {
    "api": true
  },
  "html5Mode": true,
  "editExample": true,
  "startPage": "/docs",
  "scripts": [
    "angular.js",
    "angular-animate.js"
  ]
};