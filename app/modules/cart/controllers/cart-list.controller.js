(function() {

    'use strict';

    /**
    * @ngdoc controller
    * @name app.cart.controllers:CartListController
    * @description Controller bound to the list of all the cart items
    * @requires BasketHelper
    **/
    angular
        .module('app.cart')
        .controller('CartListController', CartListController);

    CartListController.$inject = ['BasketHelper'];

    function CartListController(BasketHelper) {

        var vm = this;
        vm.selected = [];
        vm.basketHelper = BasketHelper;

        vm.removeFromBasket = BasketHelper.removeFromBasket;
        vm.addToBasket = BasketHelper.addToBasket;

        vm.cartList = vm.basketHelper.getBasket();

    }

})();
