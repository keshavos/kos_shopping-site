(function() {

    'use strict';

    /**
     * @ngdoc object
     * @name app.cart.config:cartRoutes
     * @description The config block which defines all the routes within the Cart module
     **/
    angular
        .module('app.cart')
        .config(cartRoutes);

    cartRoutes.$inject = ['$stateProvider'];

    function cartRoutes($stateProvider) {

        var cartMain = {
            name: 'cart',
            url: '/cart',
            parent: 'main',
            abstract: true
        };

        var cartList = {
            name: 'cart.list',
            url: '/list',
            parent: cartMain,
            views: {
                'mainHeader@main': {
                    templateUrl: 'app/modules/core/views/main-top-nav.html'
                },
                '@main': {
                    controller: 'CartListController',
                    controllerAs: 'vm',
                    templateUrl: 'app/modules/cart/views/cart-list.html'
                }
            }
        };

        $stateProvider
            .state(cartMain)
            .state(cartList);

    }

})();
