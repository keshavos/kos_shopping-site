(function() {

    'use strict';

    /**
     * @ngdoc directive
     * @name app.cart.directives:cartTotalButton
     * @description A button which displays the current Cart total
     * On click, this will navigate to the cart list view
     * @restrict E
     * @requires ui.router.$state
     * @requires app.product.factory:BasketHelper
     **/
    angular
        .module('app.cart')
        .directive('cartTotalButton', cartTotalButton);

    cartTotalButton.$inject = ['$state', 'BasketHelper'];

    function cartTotalButton($state, BasketHelper) {
        return {
            link: linkFn,
            restrict: 'E',
            scope: {},
            template: '<md-button class="md-raised" aria-label="view basket button" md-no-ink="true" layout="row" layout-align="space-between center" ng-disabled="!basketHelper.getBasketTotal()" ng-click="navigateToCartList()">' +
                '<md-icon class="material-icons">shopping_basket</md-icon>' +
                '<span class="md-body-2">&pound;{{  basketHelper.getBasketTotal() | number : 2 }}</span>' +
                '</md-button>'
        };

        function linkFn(scope, element, attrs) {
            scope.basketHelper = BasketHelper;

            scope.navigateToCartList = navigateToCartList;

            /**
             * @ngdoc function
             * @name app.cart.directives:cartTotalButton#navigateToCartList
             * @methodOf app.cart.directives:cartTotalButton
             * @description Navigate to the Shopping cart list view
             **/
            function navigateToCartList() {
                $state.go('cart.list');
            }
        }
    }

})();
