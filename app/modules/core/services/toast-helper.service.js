(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name app.core.factory:ToastHelper
     * @description A helper factory related to displaying 'toast' messages across the application
     * @requires ngMaterial.$mdToast
     **/
    angular
        .module('app.core')
        .factory('ToastHelper', ToastHelperFactory);

    ToastHelperFactory.$inject = ['$mdToast'];

    function ToastHelperFactory($mdToast) {

        var ToastHelper = {};
        ToastHelper.busy = false;

        /**
         * @ngdoc function
         * @name app.core.factory:ToastHelper#showToast
         * @methodOf app.core.factory:ToastHelper
         * @description Shows a simple toast message
         **/
        ToastHelper.showToast = function(message) {
            $mdToast.show(
                $mdToast.simple()
                .textContent(message)
                .position('top right')
                .hideDelay(3000)
            );
        };

        return ToastHelper;

    }

})();
