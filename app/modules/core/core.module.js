(function() {

    'use strict';

    /**
     * @ngdoc overview
     * @name app.core
     * @description The Core module
     */
    angular
        .module('app.core', [

        ]);

})();
