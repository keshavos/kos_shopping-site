(function() {

    'use strict';

    /**
     * @ngdoc object
     * @name app.core:configBlock
     * @description Config and run block
     */
    angular
        .module('app.core')
        .config(configure);

    configure.$inject = ['$locationProvider', '$httpProvider'];

    function configure($locationProvider, $httpProvider) {
        $locationProvider.hashPrefix('!');
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'; // This is required for Browser Sync to work poperly
    }

})();
