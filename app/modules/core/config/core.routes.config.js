(function() {

    'use strict';

    /**
     * @ngdoc object
     * @name app.core.config:coreRoutes
     * @description The config block of the core module which defines all the routes within the module
     **/
    angular
        .module('app.core')
        .config(coreRoutes);

    coreRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

    function coreRoutes($stateProvider, $urlRouterProvider) {

        var main = {
            name: 'main',
            url: '',
            abstract: true,
            views: {
                '': {
                    templateUrl: 'app/modules/core/views/main.html'
                }
            }
        };

        var mainViews = {
            name: 'main.views',
            parent: main,
            url: '/',
            views: {
                'mainHeader@main': {
                    templateUrl: 'app/modules/core/views/main-top-nav.html'
                }
            }
        };

        var errors = {
            name: 'errors',
            url: '/errors',
            abstract: true,
            template: '<ui-view></ui-view>'
        };

        var error500 = {
            name: '500',
            url: '/500',
            parent: 'errors',
            template: '<h1>500 Error!</h1>'
        };

        var error401 = {
            name: '401',
            url: '/401',
            parent: 'errors',
            template: '<h1>401 Error!</h1>'
        };

        $stateProvider
            .state(main)
            .state(mainViews)
            .state(errors)
            .state(error500)
            .state(error401);

        $urlRouterProvider.otherwise('/product/list');
    }

})();
