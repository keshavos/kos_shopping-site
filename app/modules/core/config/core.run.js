(function() {

    'use strict';

    /**
     * @ngdoc object
     * @name app.core.run
     * @description Adds run blocks to this module
     * @requires ng.$rootScope
     * @requires ui.router.$state
     * @requires ui.router.state.$stateParams
     */
    angular
        .module('app.core')
        .run(run)
        .run(runPageTransitions);

    run.$inject = ['$rootScope', '$state', '$stateParams'];
    runPageTransitions.$inject = ['$rootScope', '$location', '$state'];

    /**
     * @ngdoc object
     * @name  app.core.run#run
     * @methodOf app.core.run
     * @requires ng.$rootScope
     * @requires ui.router.$state
     * @requires ui.router.state.$stateParams
     * @requires material.core.$mdMedia
     * @description Add UI Router $state and $stateParams to the $rootScope for convenience
     */
    function run($rootScope, $state, $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
    }

    /**
     * @ngdoc object
     * @name app.core.run#runPageTransitions
     * @methodOf app.core.run
     * @requires ng.$rootScope
     * @requires ng.$location
     * @requires ui-router.$state
     * @description todo: show and hide page loader visual elements
     * Handle:
     * - State change start
     * - State change success
     * - State change error
     * - State not found
     */
    function runPageTransitions($rootScope, $location, $state) {
        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams) {
            });

        $rootScope.$on('$stateChangeSuccess',
            function(event, toState, toParams, fromState, fromParams) {
                $rootScope.previousState = fromState;
                $rootScope.previousParams = fromParams;
            });

        $rootScope.$on('$stateChangeError',
            function(event, toState, toParams, fromState, fromParams, error) {
            });

        $rootScope.$on('$stateNotFound',
            function(event, unfoundState, fromState, fromParams) {
                $state.go('errros.401');
            });
    }

})();
