(function() {

    'use strict';

    /**
     * @ngdoc directive
     * @name app.core.directives:closeModal
     * @description A button directive that closes the $mdDialog modal
     * @restrict E
     **/
    angular
        .module('app.core')
        .directive('closeModal', closeModal);

    closeModal.$inject = ['$mdDialog'];

    function closeModal($mdDialog) {

        return {
            link: link,
            restrict: 'E',
            scope: {},
            template: '<md-button class="md-icon-button" ng-click="closeModalFn()">' +
                '<md-icon class="material-icons" aria-label="Close dialog">close</md-icon>' +
                '</md-button>'
        };

        function link(scope, element, attrs) {
            scope.closeModalFn = closeModalFn;

            /**
             * @ngdoc function
             * @name app.core.directives:closeModal#closeModalFn
             * @methodOf app.core.directives:closeModal
             * @description Call the $mdDialog cance button to close the dialog window
             **/
            function closeModalFn() {
                $mdDialog.cancel();
            }
        }

    }

})();
