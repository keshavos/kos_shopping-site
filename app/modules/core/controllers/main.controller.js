(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name app.core.controllers:MainController
     * @description The main controller which displays a list of all items
     * @requires app.product.factory:ProductHelper
     **/
    angular
        .module('app.core')
        .controller('MainController', MainController);

    MainController.$inject = ['ProductHelper'];

    function MainController(ProductHelper) {

        var vm = this;
        vm.productHelper = ProductHelper;

        vm.init = init;

        vm.init();

        /**
         * @ngdoc function
         * @name app.core.controllers:MainController#init
         * @methodOf app.core.controllers:MainController
         * @description Fetch the product list on load
         **/
        function init() {
            ProductHelper
                .getProductsList()
                .then(function(results) {
                    vm.products = results;
                }, function(errors) {
                    // @todo: handle error state gracefully/ retry
                    console.log('errors', errors);
                });
        }

    }

})();
