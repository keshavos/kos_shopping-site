(function() {

    'use strict';

    /**
     * @ngdoc directive
     * @name app.product.directives:productTile
     * @description A tile which displays the "closed state" of a product
     * @restrict EA
     * @requires app.product.factory:BasketHelper
     * @requires app.product.factory:ProductModals
     **/
    angular
        .module('app.product')
        .directive('productTile', productTile);

    productTile.$inject = ['BasketHelper', 'ProductModals'];

    function productTile(BasketHelper, ProductModals) {

        return {
            link: link,
            replace: true,
            restrict: 'E',
            templateUrl: 'app/modules/product/views/product-tile.html'
        };

        function link(scope, element, attrs) {
            scope.addToBasket = BasketHelper.addToBasket;
            // scope.viewProduct = ProductModals.viewProduct;
        }

    }

})();
