(function() {

    'use strict';

    /**
     * @ngdoc directive
     * @name app.product.directives:addToBasket
     * @description A directive button which adds the specified product along with the quantity to the Basket
     * @restrict E
     * @requires app.product.factory:BasketHelper
     * @requires app.product.factory:ProductHelper
     * @requires app.core.factory:ToastHelper
     **/
    angular
        .module('app.product')
        .directive('addToBasket', addToBasket);

    addToBasket.$inject = ['BasketHelper', 'ProductHelper'];

    function addToBasket(BasketHelper, ProductHelper) {
        return {
            link: linkFn,
            restrict: 'E',
            scope: {
                product: '=',
                quantity: '='
            },
            template: '<md-button class="md-raised product-buy_button" aria-label="Buy button" md-no-ink="true" layout="row" layout-align="center center" ' +
                'ng-click="addToBasketFn();$event.stopPropagation();" ng-disabled="!product.quantity || btnDisabledState">' +
                '<span class="md-body-1" ng-if="product.quantity">Buy</span><span class="md-body-1" ng-if="!product.quantity">Out of Stock</span></md-button>'
        };

        function linkFn(scope, element, attrs) {
            scope.btnDisabledState = false;

            scope.setBtnDisabledState = setBtnDisabledState;
            scope.addToBasketFn = addToBasketFn;

            // run on init
            setBtnDisabledState();

            /**
             * @ngdoc function
             * @name app.product.directives:addToBasket
             * @methodOf app.product.directives:addToBasket
             * @description Sets the enabled/ disabled state on the button
             **/
            function setBtnDisabledState() {
                var productFromInventory = ProductHelper.getProduct(scope.product);
                var productFromBasket = BasketHelper.getProductFromBasket(scope.product);

                // Specified Quantity will be greater than inventory, do not add to basket
                if (productFromInventory && productFromBasket && (productFromBasket.quantity + 1) > productFromInventory.quantity) {
                    scope.btnDisabledState = true;
                } else {
                    scope.btnDisabledState = false;
                }
            }

            /**
             * @ngdoc function
             * @name app.product.directives:addToBasket
             * @methodOf app.product.directives:addToBasket
             * @description Adds 1 item (of the specified product) to the basket
             **/
            function addToBasketFn() {
                BasketHelper.addToBasket(scope.product, 1);
                setBtnDisabledState();
            }
        }

    }

})();
