(function() {

    'use strict';

    /**
     * @ngdoc object
     * @name app.product.config:productRoutes
     * @description Config block which defines all the routes within the 'Product' module
     **/
    angular
        .module('app.product')
        .config(productRoutes);

    productRoutes.$inject = ['$stateProvider'];

    function productRoutes($stateProvider) {

        var productMain = {
            name: 'product',
            url: '/product',
            parent: 'main',
            abstract: true
        };

        var productList = {
            name: 'product.list',
            parent: productMain,
            url: '/list',
            views: {
                'mainHeader@main': {
                    templateUrl: 'app/modules/core/views/main-top-nav.html'
                },
                '@main': {
                    controller: 'MainController',
                    controllerAs: 'vm',
                    templateUrl: 'app/modules/product/views/product-list.html'
                }
            }
        };

        var productView = {
            name: 'product.view',
            url: '/view/:id',
            parent: productMain,
            template: '<h1>P View</h1>',
            onEnter: ['$scope', function($scope) {
                console.log('open modal dialog here');
            }],
            onExit: ['$scope', function($scope) {
                console.log('close modal dialog here');
            }]
        };

        $stateProvider
            .state(productMain)
            .state(productList)
            .state(productView);

    }

})();
