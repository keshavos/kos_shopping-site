(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name app.product.factory:BasketHelper
     * @description A helper which deals with all 'Basket' related functionality
     * @requires app.product.factory:ProductQuery
     * @requires app.core.factory:ToastHelper
     **/
    angular
        .module('app.product')
        .factory('BasketHelper', BasketHelperFactory);

    BasketHelperFactory.$inject = ['ProductHelper', 'ToastHelper'];

    function BasketHelperFactory(ProductHelper, ToastHelper) {

        var BasketHelper = {};
        var _basketTotal = 0;
        var _basket = [];

        /**
         * @ngdoc function
         * @name app.product.factory:BasketHelper#getBasketTotal
         * @methodOf app.product.factory:BasketHelper
         * @description Get the total value of the basket
         **/
        BasketHelper.getBasketTotal = function() {
            return _basketTotal;
        };

        /**
         * @ngdoc function
         * @name app.product.factory:BasketHelper#getBasketTotalQuantity
         * @methodOf app.product.factory:BasketHelper
         * @description Get the total quantity of items in the basket
         **/
        BasketHelper.getBasketTotalQuantity = function(product) {
            var totalQuantity = 0;
            angular.forEach(_basket, function(value, key) {
                totalQuantity += value.quantity;
            });
            return totalQuantity;
        };

        /**
         * @ngdoc function
         * @name app.product.factory:BasketHelper#getBasket
         * @methodOf app.product.factory:BasketHelper
         * @description Get the basket list
         **/
        BasketHelper.getBasket = function() {
            return _basket;
        };

        /**
         * @ngdoc function
         * @name app.product.factory:BasketHelper#getProductFromBasket
         * @methodOf app.product.factory:BasketHelper
         * @description Returns the specified product, if found, from the basket list
         * @param {object} product The Product object
         **/
        BasketHelper.getProductFromBasket = function(product) {
            var elementPos = _basket.map(function(x) { return x.id; }).indexOf(product.id);
            var productFound = _basket[elementPos];
            return productFound;
        };

        /**
         * @ngdoc function
         * @name app.product.factory:BasketHelper#addToBasket
         * @methodOf app.product.factory:BasketHelper
         * @description Adds an item to the basket if it passes all requirements and checks
         * @param {object} product The Product object
         * @param {number} quantity Quantity of the product to be added to the basket
         **/
        BasketHelper.addToBasket = function(product, quantity) {
            if (!product) {
                return;
            }

            var productFromInventory = ProductHelper.getProduct(product);
            var productFromBasket = BasketHelper.getProductFromBasket(product);

            // Specified Quantity will be greater than inventory, do not add to basket
            if (productFromInventory && productFromBasket && (productFromBasket.quantity + quantity) > productFromInventory.quantity) {
                ToastHelper.showToast('Sorry! Maximum number of ' + productFromInventory.name + ' available is ' + productFromInventory.quantity);
                return false;
            }

            BasketHelper.updateBasket(product, quantity);
            return _basket;
        };

        /**
         * @ngdoc function
         * @name app.product.factory:BasketHelper#removeFromBasket
         * @methodOf app.product.factory:BasketHelper
         * @description Removes the specified number of an item from the basket
         * @param {object} product The Product object
         * @param {number} quantity Quantity of the product to be removed from the basket
         **/
        BasketHelper.removeFromBasket = function(product, quantity) {
            if (!product || quantity === null || angular.isUndefined(quantity)) {
                return;
            }

            BasketHelper.updateBasket(product, quantity);
            return _basket;
        };

        /**
         * @ngdoc function
         * @name app.product.factory:BasketHelper#updateBasket
         * @methodOf app.product.factory:BasketHelper
         * @description Updates the specified product in basket list with the specified quantity
         * @param {object} product The Product object
         * @param {number} quantity Quantity of the product to be removed from the basket
         **/
        BasketHelper.updateBasket = function(product, quantity) {
            var elementPos = _basket.map(function(x) { return x.id; }).indexOf(product.id);
            var objectFound = _basket[elementPos];
            var productToAdd = {};
            angular.extend(productToAdd, product);

            if (objectFound) {
                _basket[elementPos].quantity += quantity;
            } else {
                productToAdd.quantity = quantity;
                _basket.push(productToAdd);
            }

            // need to do this again to remove any products with 0 quantity
            elementPos = _basket.map(function(x) { return x.id; }).indexOf(product.id);
            objectFound = _basket[elementPos];

            if (!objectFound.quantity) {
                _basket.splice(elementPos, 1);
            }

            BasketHelper.updateBasketTotal(product, quantity);
        };

        /**
         * @ngdoc function
         * @name app.product.factory:BasketHelper#updateBasketTotal
         * @methodOf app.product.factory:BasketHelper
         * @description Updates the basketTotal with the specified quantity of the product
         * @param {object} product The Product object
         * @param {number} quantity Quantity of the product
         **/
        BasketHelper.updateBasketTotal = function(product, quantity) {
            if (quantity) {
                _basketTotal += (product.price * quantity);
            } else {
                _basketTotal -= (product.price * quantity);
            }
            (Math.round(_basketTotal * 100) / 100).toFixed(2);
        };

        return BasketHelper;

    }

})();
