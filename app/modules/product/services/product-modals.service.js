(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name app.product.factory:ProductModals
     * @description A modals helper factory for the Product module
     **/
    angular
        .module('app.product')
        .factory('ProductModals', ProductModalsFactory);

    ProductModalsFactory.$inject = ['$mdDialog'];

    function ProductModalsFactory($mdDialog) {

        var ProductModals = {};

        /**
         * @ngdoc function
         * @name app.product.factory:ProductModals#viewProduct
         * @methodOf app.product.factory:ProductModals
         * @description Opens the product detail dialog window
         **/
        ProductModals.viewProduct = function(product) {
            if (!product) {
                return;
            }

            var modalOptions = {
                parent: angular.element(document.body),
                templateUrl: 'app/modules/product/views/product-view.modal.html',
                controller: 'ProductViewController',
                controllerAs: 'vm',
                resolve: {
                    product: function() {
                        return product;
                    }
                }
            };

            return $mdDialog.show(modalOptions);
        };

        return ProductModals;
    }

})();
