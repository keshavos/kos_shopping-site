(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name app.product.factory:ProductHelper
     * @description A helper factory which serves product related data from the mocks file
     * @requires ng.$http
     * @requires ng.$q
     **/
    angular
        .module('app.product')
        .factory('ProductHelper', ProductHelperFactory);

    ProductHelperFactory.$inject = ['$http', '$q'];

    function ProductHelperFactory($http, $q) {

        var ProductHelper = {};
        var _productMocksFileLocation = 'app/modules/product/mocks/product-inventory.json';
        var _productsList = [];

        /**
         * @ngdoc function
         * @name app.product.factory:ProductHelper#getProductsList
         * @methodOf app.product.factory:ProductHelper
         * @description Returns the inventory list
         **/
        ProductHelper.getProductsList = function() {
            return $http.get(_productMocksFileLocation)
                .then(function(success) {
                    _productsList = success.data;
                    return _productsList;
                }, function(errors) {
                    // @todo: handle this scenario on the FE
                    console.log('error fetching products list');
                });
        };

        /**
         * @ngdoc function
         * @name app.product.factory:ProductHelper#getProduct
         * @methodOf app.product.factory:ProductHelper
         * @description Returns the matched product object from the inventory list
         * @param {object} product The Product object
         **/
        ProductHelper.getProduct = function(product) {
            var elementPos = _productsList.map(function(x) { return x.id; }).indexOf(product.id);
            var productFound = _productsList[elementPos];
            return productFound;
        };

        return ProductHelper;
    }

})();
