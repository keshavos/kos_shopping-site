(function() {

    'use strict';

    /**
    * @ngdoc controller
    * @name app.product.controllers:ProductViewController
    * @description Controller bound to the view to display a single Product
    **/
    angular
        .module('app.product')
        .controller('ProductViewController', ProductViewController);

    ProductViewController.$inject = ['product'];

    function ProductViewController(product) {

        var vm = this;
        vm.product = product;
    }

})();
