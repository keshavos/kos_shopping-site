describe(':app.product.factory:BasketHelper', function() {

    'use strict';

    var test_basketHelper,
        test_basketHelperInstance;

    beforeEach(function() {
        module('ui.router');
        module('ngMaterial');
        module('app.core');
        module('app.product');

        inject(function(_BasketHelper_) {
            test_basketHelper = _BasketHelper_;
        });

        test_basketHelperInstance = test_basketHelper;
    });

    it('should verify all the methods on the factory', function() {
        expect(angular.isFunction(test_basketHelperInstance.getBasketTotal)).toBeTruthy();
        expect(angular.isFunction(test_basketHelperInstance.getBasket)).toBeTruthy();
        expect(angular.isFunction(test_basketHelperInstance.getProductFromBasket)).toBeTruthy();
        expect(angular.isFunction(test_basketHelperInstance.addToBasket)).toBeTruthy();
        expect(angular.isFunction(test_basketHelperInstance.removeFromBasket)).toBeTruthy();
        expect(angular.isFunction(test_basketHelperInstance.updateBasket)).toBeTruthy();
        expect(angular.isFunction(test_basketHelperInstance.updateBasketTotal)).toBeTruthy();
    });

    it('should return 0 value of the basketTotal', function() {
        expect(test_basketHelperInstance.getBasketTotal()).toBe(0);
    });

    describe(':getBasketTotal()', function() {
        it('_basket should be of Array type', function() {
            expect(test_basketHelperInstance.getBasketTotal()).toBe(0);
        });
    });

    describe(':getBasket()', function() {
        it('should return _basket', function() {
            expect(Array.isArray(test_basketHelperInstance.getBasket())).toBeTruthy();
        });
        it('_basket an empty array', function() {
            expect(test_basketHelperInstance.getBasket().length).toBe(0);
        });
    });

    describe(':getProductFromBasket()', function() {
        it('should return undefined from _basket if a specified product is not found', function() {
            expect(test_basketHelperInstance.getBasket().length).toBe(0);
            var inputProduct = { id: 1, name: 'test_product', price: 10 };
            test_basketHelperInstance.addToBasket(inputProduct, 1);
            expect(test_basketHelperInstance.getBasket().length).toBe(1);
            expect(test_basketHelperInstance.getProductFromBasket({ id: 999, name: 'hello', price: 10 })).toBe(undefined);
        });
        it('should return product from _basket if a specified product is found', function() {
            var inputProduct = { id: 1, name: 'test_product', price: 10, quantity: 1 };
            expect(test_basketHelperInstance.getBasket().length).toBe(0);
            test_basketHelperInstance.addToBasket(inputProduct, 1);
            expect(test_basketHelperInstance.getBasket().length).toBe(1);
            expect(test_basketHelperInstance.getProductFromBasket(inputProduct)).toEqual(inputProduct);
        });
    });

    describe(':addToBasket()', function() {
        it('should return if product is not passed in param', function() {
            expect(test_basketHelperInstance.getBasketTotal()).toBe(0);
            test_basketHelperInstance.addToBasket();
            expect(test_basketHelperInstance.getBasketTotal()).toBe(0);
        });

        it('should return if product is null', function() {
            expect(test_basketHelperInstance.getBasketTotal()).toBe(0);
            test_basketHelperInstance.addToBasket(null);
            expect(test_basketHelperInstance.getBasketTotal()).toBe(0);
        });

        it('should return increment basketTotal with the product price', function() {
            var inputProduct = { name: 'test_product', price: 10 };
            expect(test_basketHelperInstance.getBasketTotal()).toBe(0);
            test_basketHelperInstance.addToBasket(inputProduct, 1);
            expect(test_basketHelperInstance.getBasketTotal()).toBe(10);
        });
    });

    describe(':removeFromBasket()', function() {
        it('should return if product is not passed in param', function() {
            expect(test_basketHelperInstance.getBasketTotal()).toBe(0);
            var result = test_basketHelperInstance.removeFromBasket();
            expect(test_basketHelperInstance.getBasketTotal()).toBe(0);
        });

        it('should return if product is null', function() {
            expect(test_basketHelperInstance.getBasketTotal()).toBe(0);
            var result = test_basketHelperInstance.removeFromBasket(null);
            expect(test_basketHelperInstance.getBasketTotal()).toBe(0);
        });

        it('should return decrement basketTotal with the product price', function() {
            var inputProduct = { name: 'test_product', price: 10 };
            expect(test_basketHelperInstance.getBasketTotal()).toBe(0);
            test_basketHelperInstance.addToBasket(inputProduct, 1);
            expect(test_basketHelperInstance.getBasketTotal()).toBe(10);
            test_basketHelperInstance.removeFromBasket(inputProduct, -1);
            expect(test_basketHelperInstance.getBasketTotal()).toBe(0);
        });
    });

});
