describe(':app.product.factory:ProductHelper', function() {

    'use strict';

    var test_productHelper,
        test_productHelperInstance;

    beforeEach(function() {
        module('ui.router');
        module('ngMaterial');
        module('app.core');
        module('app.product');

        inject(function(_ProductHelper_) {
            test_productHelper = _ProductHelper_;
        });

        test_productHelperInstance = test_productHelper;
    });

    it('should verify all the methods on the factory', function() {
        expect(angular.isFunction(test_productHelperInstance.getProductsList)).toBeTruthy();
        expect(angular.isFunction(test_productHelperInstance.getProduct)).toBeTruthy();
    });

    describe(':getProductsList()', function() {
    });

    describe(':getProduct()', function() {
    });

});
