(function() {

    'use strict';

    /**
     * @ngdoc overview
     * @name app
     * @description Main module of the application.
     * @requires ngResource
     * @requires ngAria
     * @requires ngMaterial
     * @requires ngMessages
     * @requires ngAnimate
     * @requires ngSanitize
     * @requires md.data.table
     * @requires ui.router
     * @requires app.cart
     * @requires app.core
     * @requires app.product
     */
    angular.module('app', [
        'ngResource',
        'ngAria',
        'ngMaterial',
        'ngMessages',
        'ngAnimate',
        'ngSanitize',
        'md.data.table',
        'ui.router',
        'app.cart',
        'app.core',
        'app.product'
    ]);

})();
