'use strict';

// Karma configuration
module.exports = function(config) {
    config.set({
        // Frameworks to use
        frameworks: ['jasmine'],

        // List of files / patterns to load in the browser
        files: [
            <!-- injector:bower-js -->
            'src/bower_components/es5-shim/es5-shim.js',
            'src/bower_components/json3/lib/json3.min.js',
            'src/bower_components/angular/angular.js',
            'src/bower_components/angular-mocks/angular-mocks.js',
            'src/bower_components/angular-animate/angular-animate.js',
            'src/bower_components/angular-resource/angular-resource.js',
            'src/bower_components/angular-messages/angular-messages.js',
            'src/bower_components/angular-aria/angular-aria.js',
            'src/bower_components/angular-material/angular-material.js',
            'src/bower_components/angular-ui-router/release/angular-ui-router.js',
            'src/bower_components/angular-sanitize/angular-sanitize.js',
            'src/bower_components/angular-material-data-table/dist/md-data-table.js',
            <!-- endinjector -->
            <!-- injector:js -->
            'app/modules/cart/cart.module.js',
            'app/modules/core/core.module.js',
            'app/modules/product/product.module.js',
            'app/app.js',
            'app/modules/cart/config/cart.routes.config.js',
            'app/modules/core/config/core.config.js',
            'app/modules/core/config/core.routes.config.js',
            'app/modules/core/config/core.run.js',
            'app/modules/product/config/product.routes.config.js',
            'app/modules/cart/controllers/cart-list.controller.js',
            'app/modules/core/controllers/main.controller.js',
            'app/modules/product/controllers/product-view.controller.js',
            'app/modules/cart/directives/cart-total-button.directive.js',
            'app/modules/core/directives/close-modal.directive.js',
            'app/modules/product/directives/add-to-basket.directive.js',
            'app/modules/product/directives/product-tile.directive.js',
            'app/modules/core/services/toast-helper.service.js',
            'app/modules/product/services/basket-helper.service.js',
            'app/modules/product/services/product-helper.service.js',
            'app/modules/product/services/product-modals.service.js',
            'app/modules/product/tests/unit/basket-helper.service.spec.js',
            'app/modules/product/tests/unit/product-helper.service.spec.js',
            <!-- endinjector -->
        ],

        // Test results reporter to use
        // Possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
        //reporters: ['progress'],
        reporters: ['spec', 'coverage'],

        preprocessors: {
            'app/modules/*/config/*.js': 'coverage',
            'app/modules/*/controllers/*.js': 'coverage',
            'app/modules/*/directives/*.js': 'coverage',
            'app/modules/*/filters/*.js': 'coverage',
            'app/modules/*/services/*.js': 'coverage'
        },

        // Web server port
        port: 9876,

        // Enable / disable colors in the output (reporters and logs)
        colors: true,

        // Level of logging
        // Possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,

        // Enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,

        // Start these browsers, currently available:
        // - Chrome
        // - ChromeCanary
        // - Firefox
        // - Opera
        // - Safari (only Mac)
        // - PhantomJS
        // - IE (only Windows)
        browsers: ['PhantomJS'],

        // If browser does not capture in given timeout [ms], kill it
        captureTimeout: 60000,

        // Continuous Integration mode
        // If true, it capture browsers, run tests and exit
        singleRun: true
    });
};
